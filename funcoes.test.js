import $ from 'jquery';
const sum = require('./funcoes');

test('adds 1 + 2 to equal 3', () => {

    document.body.innerHTML = '<div id="myDiv"></div>';
    $('#myDiv').text('Hello, jQuery!');

    expect(sum(1, 2)).toBe(3);
});